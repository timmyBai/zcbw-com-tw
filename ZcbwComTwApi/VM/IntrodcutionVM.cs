﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZcbwComTwApi.VM
{
    public class IntrodcutionInfoVM
    {
        public IntrodcutionInfoVM()
        {
            this.roles = new List<string>();
        }

        public string userName { get; set; }
        public List<string> roles { get; set; }
    }

    public class UserIntrodcutionVM
    {
        public string userName { get; set; }
        public string roles { get; set; }
        public string Exp { get; set; }
    }
}