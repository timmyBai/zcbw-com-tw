﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZcbwComTwApi.VM
{
    public class SystemPostVM
    {
        public int total { get; set; }
        public List<SystemPostListVM> systemPostInfo { get; set; }
    }

    public class SystemPostListVM
    {
        public string systemPostId { get; set; }
        public string postTitle { get; set; }
        public string postMessage { get; set; }
        public string publish { get; set; }
        public string sDate { get; set; }
        public string sTime { get; set; }
    }

    public class SystemPostTotalVM
    {
        public int total { get; set; }
    }

    public class OnlySystemPostVM
    {
        public int systemPostId { get; set; }
        public string postTitle { get; set; }
        public string postMessage { get; set; }
        public string publish { get; set; }
        public string mUser { get; set; }
    }
}