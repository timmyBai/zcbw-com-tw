﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

using Newtonsoft.Json;

using ZcbwComTwApi.Security;
using ZcbwComTwApi.Models;
using ZcbwComTwApi.VM;


namespace ZcbwComTwApi.Controllers
{
    public class IntroductionApiController : ApiController
    {
        /// <summary>
        /// 解密使用者 jwt token 資料
        /// </summary>
        /// <param name="body">jwt token 解密模型</param>
        /// <returns></returns>
        [JwtAuthFilter]
        [HttpPost]
        [Route("api/UserIntroduction")]
        [EnableCors(origins: "*", headers: "Authorization", methods: "POST")]
        public HttpResponseMessage GetUserIntroduction(SearchIntroductionForm body)
        {
            //創建新的統一回傳類型
            ResultModels result = new ResultModels();

            //檢查資料模型
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "資料傳輸錯誤";
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }

            try
            {
                //解密 jwt token
                var jwtDecodeObject = new JwtAuthUtil().JwtDecode(body.token);

                if (jwtDecodeObject == null)
                {
                    result.isSuccess = false;
                    result.message = "System call unLock failed";
                    return Request.CreateResponse(HttpStatusCode.BadRequest, result);
                }

                //序列化 jwt token 解密物件
                var serializeJwtObject = JsonConvert.SerializeObject(jwtDecodeObject);

                //反序列化 jwt token 解密物件
                var deserializeObject = JsonConvert.DeserializeObject<UserIntrodcutionVM>(serializeJwtObject);

                if (deserializeObject == null)
                {
                    result.isSuccess = false;
                    result.message = "System call unLock failed!";
                    return Request.CreateResponse(HttpStatusCode.BadRequest, result);
                }

                //創建新使用者資訊類別
                IntrodcutionInfoVM introdcutionInfo = new IntrodcutionInfoVM();
                introdcutionInfo.roles.Add(deserializeObject.roles);
                introdcutionInfo.userName = deserializeObject.userName;

                //回傳資料
                result.isSuccess = true;
                result.data = introdcutionInfo;
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }
}
