﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http;

using System.Web.Http;
using System.Web.Http.Cors;

using System.IO;

using MySql.Data.MySqlClient;
using ClosedXML.Excel;

using ZcbwComTwApi.Models;
using ZcbwComTwApi.VM;
using ZcbwComTwApi.Security;

namespace ZcbwComTwApi.Controllers
{
    public class ClassRoomApiController : ApiController
    {
        private MySqlConnection conn;

        /// <summary>
        /// 查詢班務資料，基本搜尋資料模式
        /// </summary>
        /// <param name="body">查詢班務資料，基本搜尋資料模型</param>
        /// <returns></returns>
        [JwtAuthFilter]
        [HttpPost]
        [Route("api/FetchClassRoomBase")]
        [EnableCors(origins: "*", headers: "Authorization", methods: "POST")]
        public HttpResponseMessage GetClassRoomBase(SearchClassRoomBaseForm body)
        {
            //創建新的統一回傳類型
            ResultModels result = new ResultModels();

            //檢查資料模型
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "資料傳輸失敗";
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }

            try
            {
                string classes = body.classes == null ? "" : body.classes;    //組別
                string gender = body.gender == null ? "" : body.gender;       //性別: 前道、坤道
                int offset = body.page * body.itemBarMenu - body.itemBarMenu; //頁碼
                int rowCount = body.itemBarMenu;                              //筆數

                using (conn = new MySqlConnection(Settings.MysqlSettings.connStr))
                {
                    conn.Open();

                    //查詢班務基本搜尋資料
                    var classRoomList = new ClassRoomModles(conn).GetClassRoomBase<ClassRoomListVM>(classes, gender, offset, rowCount);

                    //查詢班務資料總數
                    var classRoomTotal = new ClassRoomModles(conn).GetClassRoomTotal<ClassRoomTotalVM>(classes, gender);

                    //創建班務資料會診
                    ClassRoomVM classRoom = new ClassRoomVM();
                    classRoom.classRoom = classRoomList.ToList();
                    classRoom.total = classRoomTotal.total;

                    if (classRoomList.Count() == 0)
                    {
                        result.isSuccess = false;
                        result.message = "查無班務資料";
                    }
                    else
                    {
                        result.isSuccess = true;
                        result.data = classRoom;
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// 查詢班務資料，詳細搜尋資料模式
        /// </summary>
        /// <param name="body">查詢班務資料，詳細搜尋資料模型</param>
        /// <returns></returns>
        [JwtAuthFilter]
        [HttpPost]
        [Route("api/FetchClassRoomDetail")]
        [EnableCors(origins: "*", headers: "Authorization", methods: "POST")]
        public HttpResponseMessage GetClassRoomDetail(SearchClassRoomDetailForm body)
        {
            //創建新的統一回傳類型
            ResultModels result = new ResultModels();

            //檢查資料模型
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "資料傳輸失敗";
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }

            try
            {
                string classes = body.classes == null ? "" : body.classes;     //組別
                string gender = body.gender == null ? "" : body.gender;        //性別
                string name = body.name == null ? "" : body.name;              //姓名
                int offset = body.page * body.itemBarMenu - body.itemBarMenu;  //頁碼
                int rowCount = body.itemBarMenu;                               //筆數

                using (conn = new MySqlConnection(Settings.MysqlSettings.connStr))
                {
                    conn.Open();

                    //查詢班務詳細搜尋資料
                    var classRoomList = new ClassRoomModles(conn).GetClassRoomDetail<ClassRoomListVM>(classes, gender, name, offset, rowCount);

                    //查詢班務資料總數
                    var classRoomTotal = new ClassRoomModles(conn).GetClassRoomTotal<ClassRoomTotalVM>(classes, gender);

                    //創建班務資料會診
                    ClassRoomVM classRoom = new ClassRoomVM();
                    classRoom.classRoom = classRoomList.ToList();
                    classRoom.total = classRoomTotal.total;

                    if (classRoomList.Count() == 0)
                    {
                        result.isSuccess = false;
                        result.message = "查無班務資料";
                    }
                    else
                    {
                        result.isSuccess = true;
                        result.data = classRoom;
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// 查詢一筆班務資料
        /// </summary>
        /// <param name="body">查詢班務 id 模型</param>
        /// <returns></returns>
        [JwtAuthFilter]
        [HttpPost]
        [Route("api/FetchOnlyClassRoom")]
        [EnableCors(origins: "*", headers: "Authorization", methods: "POST")]
        public HttpResponseMessage GetOnlyClassRoom(SearchClassRoomIdForm body)
        {
            //創建新的統一回傳類型
            ResultModels result = new ResultModels();

            //檢查資料模型
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "資料傳輸失敗";
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }

            try
            {
                using (conn = new MySqlConnection(Settings.MysqlSettings.connStr))
                {
                    conn.Open();

                    //查詢一筆班務資料
                    var ClassRoom = new ClassRoomModles(conn).InspactOnlyClassRoom<OnlyOneClassRoomVM>(body.classRoomId);

                    if (ClassRoom == null)
                    {
                        result.isSuccess = false;
                        result.message = "查無班務資料";
                        return Request.CreateResponse(HttpStatusCode.Forbidden, result);
                    }

                    //回傳資料
                    result.isSuccess = true;
                    result.data = ClassRoom;
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// 新增班務資料
        /// </summary>
        /// <param name="body">新增班務模型</param>
        /// <returns></returns>
        [JwtAuthFilter]
        [HttpPost]
        [Route("api/CreateClassRoom")]
        [EnableCors(origins: "*", headers: "Authorization", methods: "POST")]
        public HttpResponseMessage InsertClassRoom(InsertClassRoomForm body)
        {
            //創建新的統一回傳類型
            ResultModels result = new ResultModels();

            //檢查資料模型
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "資料傳輸失敗";
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }

            try
            {
                using (conn = new MySqlConnection(Settings.MysqlSettings.connStr))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    //檢查班務組別是否存在
                    var isClassesExist = new ClassRoomModles(conn).InspactClassRoomClasses<ClassRoomClassesVM>(body.groupes, body.groupNo, body.serialNo);

                    if (isClassesExist != null)
                    {
                        result.isSuccess = false;
                        result.message = "組別已重複，請更換組別";
                        return Request.CreateResponse(HttpStatusCode.Forbidden, result);
                    }

                    //新增班務資料
                    var insertRowClassRoom = new ClassRoomModles(conn).InsertClassRoom(body);

                    if (insertRowClassRoom == 1)
                    {
                        result.isSuccess = true;
                        result.message = "新增班務資料成功";
                        transaction.Commit();
                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "新增班務資料失敗";
                        transaction.Rollback();
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(HttpStatusCode.Created, result);
        }

        /// <summary>
        /// 更新班務資料
        /// </summary>
        /// <param name="body">更新班務模型</param>
        /// <returns></returns>
        [JwtAuthFilter]
        [HttpPatch]
        [Route("api/ReplaceClassRoom")]
        [EnableCors(origins: "*", headers: "Authorization", methods: "PATCH")]
        public HttpResponseMessage UpdateClassRoom(UpdateClassRoomForm body)
        {
            //創建新的統一回傳類型
            ResultModels result = new ResultModels();

            //檢查資料模型
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "資料傳輸失敗";
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }

            try
            {
                using (conn = new MySqlConnection(Settings.MysqlSettings.connStr))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    //檢查班務資料是否存在
                    var isClassRoomExist = new ClassRoomModles(conn).InspactOnlyClassRoom<OnlyOneClassRoomVM>(body.classRoomId);

                    if (isClassRoomExist == null)
                    {
                        result.isSuccess = false;
                        result.message = "班務資料已不存在";
                        return Request.CreateResponse(HttpStatusCode.Forbidden, result);
                    }

                    //更新班務資料
                    var updateRowClassRoom = new ClassRoomModles(conn).UpdataClassRoom(body);

                    if (updateRowClassRoom == 1)
                    {
                        result.isSuccess = true;
                        result.message = "更新班務資料成功";
                        transaction.Commit();
                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "更新班務資料失敗";
                        transaction.Rollback();
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// 刪除班務資料
        /// </summary>
        /// <param name="classRoomId">刪除班務 id</param>
        /// <returns></returns>
        [JwtAuthFilter]
        [HttpDelete]
        [Route("api/CancelClassRoom/{classRoomId?}")]
        [EnableCors(origins: "*", headers: "Authorization", methods: "DELETE")]
        public HttpResponseMessage DeleteClassRoom(int classRoomId)
        {
            //創建新的統一回傳類型
            ResultModels result = new ResultModels();

            //檢查資料模型
            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "資料傳輸失敗";
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }

            try
            {
                using (conn = new MySqlConnection(Settings.MysqlSettings.connStr))
                {
                    conn.Open();
                    MySqlTransaction transaction = conn.BeginTransaction();

                    //檢查班務資料是否存在
                    var isClassRoomExist = new ClassRoomModles(conn).InspactOnlyClassRoom<OnlyOneClassRoomVM>(classRoomId);

                    if (isClassRoomExist == null)
                    {
                        result.isSuccess = false;
                        result.message = "班務資料已不存在";
                        return Request.CreateResponse(HttpStatusCode.Forbidden, result);
                    }

                    //刪除班務資料
                    var deleteRowClassRoom = new ClassRoomModles(conn).DeleteClassRoom(classRoomId);

                    if (deleteRowClassRoom == 1)
                    {
                        result.isSuccess = true;
                        result.message = "刪除班務資料成功";
                        transaction.Commit();
                    }
                    else
                    {
                        result.isSuccess = false;
                        result.message = "刪除班務資料失敗";
                        transaction.Rollback();
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [JwtAuthFilter]
        [HttpPost]
        [Route("api/ReportClassRoom")]
        [EnableCors(origins: "*", headers: "Authorization", methods: "POST")]
        public HttpResponseMessage ReportClassRoom(ReportClassRoomForm body)
        {
            dynamic response = null;
            ResultModels result = new ResultModels();

            if (!ModelState.IsValid)
            {
                result.isSuccess = false;
                result.message = "資料傳輸錯誤";
                return Request.CreateResponse(HttpStatusCode.BadRequest, result);
            }

            try
            {
                string classes = body.classes == null ? "" : body.classes;     //組別
                string gender = body.gender == null ? "" : body.gender;        //性別
                string name = body.name == null ? "" : body.name;              //姓名

                using (conn = new MySqlConnection(Settings.MysqlSettings.connStr))
                {
                    conn.Open();

                    var classRoomList = new ClassRoomModles(conn).ReportClassRoom<ReportClassRoomVM>(classes, gender, name).ToList();

                    //生成 excel 類型
                    XLWorkbook workbook = new XLWorkbook();
                    IXLWorksheet classRoomReportTables = workbook.Worksheets.Add("班務清單報表"); //清增

                    //匯出欄位
                    string[] classRoomField = new string[] { "上課地點", "組別", "姓名", "性別", "班別", "所屬佛院", "所屬區別" };

                    //新增班務欄位
                    for (int i = 0; i < classRoomField.Length; i++)
                    {
                        classRoomReportTables.Cell(1, i + 1).Value = classRoomField[i];
                    }

                    //新增班務資料
                    for (int i = 0; i < classRoomList.Count(); i++)
                    {
                        classRoomReportTables.Cell(i + 2, 1).Value = classRoomList[i].classLocation;
                        classRoomReportTables.Cell(i + 2, 2).Value = classRoomList[i].groupes;
                        classRoomReportTables.Cell(i + 2, 3).Value = classRoomList[i].name;
                        classRoomReportTables.Cell(i + 2, 4).Value = classRoomList[i].gender;
                        classRoomReportTables.Cell(i + 2, 5).Value = classRoomList[i].classes;
                        classRoomReportTables.Cell(i + 2, 6).Value = classRoomList[i].buddhaHall;
                        classRoomReportTables.Cell(i + 2, 7).Value = classRoomList[i].district;
                    }

                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        workbook.SaveAs(memoryStream); //把做完的 excel 放到 memoryStream 裡
                        result.isSuccess = true;
                        result.message = "匯出班務資料成功";
                        response = Request.CreateResponse(HttpStatusCode.OK, result);
                        response.Content = new ByteArrayContent(memoryStream.ToArray());
                        response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                        response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                        response.Content.Headers.ContentDisposition.FileName = $"test{ DateTime.Now}.xlsx";
                        response.Content.Headers.ContentLength = memoryStream.Length; //這行會告知瀏覽器我們檔案的大小
                    }
                }
            }
            catch (Exception ex)
            {
                result.isSuccess = false;
                result.message = ex.ToString();
                return Request.CreateResponse(HttpStatusCode.InternalServerError, result);
            }

            return response;
        }
    }
}
