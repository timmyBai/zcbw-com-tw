﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

using Jose;

namespace ZcbwComTwApi.Security
{
    public class JwtAuthUtil
    {
        public string GenerateToken(string userName, string roles)
        {
            string secret = "myJwtAuthDemo";//加解密的key,如果不一樣會無法成功解密
            Dictionary<string, object> claim = new Dictionary<string, object>();//payload 需透過token傳遞的資料
            claim.Add("userName", userName);
            claim.Add("roles", roles);
            claim.Add("Exp", DateTime.Now.AddSeconds(Convert.ToInt32("100")).ToString());
            var payload = claim;
            var token = JWT.Encode(payload, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS512);//產生token
            return token;
        }

        public object JwtDecode(string token)
        {
            string secret = "myJwtAuthDemo";

            var jwtObject = JWT.Decode<Dictionary<string, object>>(token, Encoding.UTF8.GetBytes(secret), JwsAlgorithm.HS512);

            return jwtObject;
        }
    }
}