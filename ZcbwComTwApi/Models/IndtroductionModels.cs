﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

using Dapper;

using MySql.Data.MySqlClient;

namespace ZcbwComTwApi.Models
{
    public class IntroductionModels
    {
        private MySqlConnection conn;
        private string sqlStr = string.Empty;

        public IntroductionModels(MySqlConnection _conn)
        {
            conn = _conn;
        }
    }

    /*
     ** 接收模型
     */

    /// <summary>
    /// 查詢 jwt token 解密
    /// </summary>
    public class SearchIntroductionForm
    {
        /// <summary>
        /// jwt token
        /// </summary>
        [Required]
        public string token { get; set; }
    }
}