﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZcbwComTwApi.Models
{
    public class ResultModels
    {
        public ResultModels()
        {
            data = new object();
        }

        public bool isSuccess { get; set; }
        public string message { get; set; }
        public object data { get; set; }
    }
}