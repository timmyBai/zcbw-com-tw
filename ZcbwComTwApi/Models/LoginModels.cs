﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

using MySql.Data.MySqlClient;
using Dapper;

namespace ZcbwComTwApi.Models
{
    public class LoginUserModels
    {
        private MySqlConnection conn;
        private string sqlStr = string.Empty;
        
        public LoginUserModels(MySqlConnection _conn)
        {
            conn = _conn;
        }

        /// <summary>
        /// 檢查帳號密碼有無停用
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="account">帳號</param>
        /// <param name="password">密碼</param>
        /// <returns></returns>
        public T InspactLoginUserApStatus<T>(string account, string password)
        {
            sqlStr = @"SELECT account
                         FROM user WHERE     account  = @account
                                         AND password = @password
                                         AND status   = 0
            ";

            object whereKey = new {
                account = account,
                password = password
            };

            var result = conn.Query<T>(sqlStr, whereKey).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 查詢使用者角色
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="account">帳號</param>
        /// <param name="password">密碼</param>
        /// <returns></returns>
        public T GetLoginUserInfo<T>(string account, string password)
        {
            sqlStr = @"SELECT userName, roles FROM user WHERE     account  = @account
                                                              AND password = @password
            ";

            object whereKey = new {
                account = account,
                password = password
            };

            var result = conn.Query<T>(sqlStr, whereKey).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 更新登入日期、時間
        /// </summary>
        /// <param name="account">帳號</param>
        /// <param name="password">密碼</param>
        /// <returns></returns>
        public int UpdateLoginUser(string account, string password)
        {
            string nowDate = DateTime.Now.ToString("yyyyMMdd");
            string nowTime = DateTime.Now.ToString("HHmmss");

            sqlStr = @"UPDATE user SET loginDate  = @loginDate,
                                       loginTime  = @loginTime WHERE     account  = @account
                                                                     AND password = @password
            ";

            object whereParam = new {
                loginDate = nowDate,
                loginTime = nowTime,
                account = account,
                password = password
            };

            int rowCount = conn.Execute(sqlStr, whereParam);

            return rowCount;
        }
    }

    /*
     ** 接收模型
     */

    /// <summary>
    /// 查詢使用者帳號
    /// </summary>
    public class SearchLoginUserForm
    {
        /// <summary>
        /// 帳號
        /// </summary>
        [Required]
        [RegularExpression("[A-Za-z0-9]{1,50}$")]
        public string account { get; set; }

        /// <summary>
        /// 密碼
        /// </summary>
        [Required]
        [RegularExpression("[A-Za-z0-9]{1,50}$")]
        public string password { get; set; }
    }
}