﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

using Dapper;
using MySql.Data.MySqlClient;

namespace ZcbwComTwApi.Models
{
    public class ClassRoomModles
    {
        private MySqlConnection conn;
        private string sqlStr = string.Empty;

        public ClassRoomModles(MySqlConnection _conn)
        {
            conn = _conn;
        }

        /// <summary>
        /// 查詢班務資料，基本搜尋模式
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="classes">組別</param>
        /// <param name="gender">性別</param>
        /// <param name="offset">頁碼</param>
        /// <param name="rowCount">筆數</param>
        /// <returns></returns>
        public IEnumerable<T> GetClassRoomBase<T>(string classes, string gender, int offset, int rowCount)
        {
            string strClassesWhereKey = (classes != "") ? "WHERE classes = @classes " : "WHERE classes IS NOT NULL ";
            string strGenderWhereKey = (gender != "") ? "AND gender = @gender " : "AND gender IS NOT NULL ";
            object whereKey = new {
                classes = classes,
                gender = gender
            };

            sqlStr = $@"SELECT classRoomId,
                               classLocation,
                               CONCAT(groupes, groupNO , '-' , serialNO) AS groupes,
                               name,
                               CASE gender
                                 WHEN 1 THEN '乾道'
                                 ELSE '坤道'
                               END AS gender,
                               CASE classes
                                 WHEN 1 THEN '新明'
                                 WHEN 2 THEN '至善'
                                 WHEN 3 THEN '培德'
                                 WHEN 4 THEN '行德'
                                 ELSE '崇德'
                               END AS classes,
                               buddhaHall,
                               district
                          FROM classRoom
                          {strClassesWhereKey}
                          {strGenderWhereKey}
                          ORDER BY sDate DESC, sTime DESC
                          LIMIT {offset}, {rowCount}
            ";

            var result = conn.Query<T>(sqlStr, whereKey).ToList();

            return result;
        }

        /// <summary>
        /// 查詢班務資料，詳細搜尋模式
        /// </summary>
        /// <param name="classes">組別</param>
        /// <param name="gender">性別</param>
        /// <param name="name">姓名</param>
        /// <param name="offset">頁碼</param>
        /// <param name="rowCount">筆數</param>
        /// <returns></returns>
        public IEnumerable<T> GetClassRoomDetail<T>(string classes, string gender, string name, int offset, int rowCount)
        {
            string strClassesWhereKey = (classes != "") ? "WHERE classes = @classes " : "WHERE classes IS NOT NULL ";
            string strGenderWhereKey = (gender != "") ? "AND gender = @gender " : "AND gender IS NOT NULL ";
            string strNameWhereKey = (name != "") ? "AND LOCATE(UPPER(@name), name) " : "AND name IS NOT NULL ";
            object whereKey = new {
                classes = classes,
                gender = gender,
                name = name
            };

            sqlStr = $@"SELECT classRoomId,
                               classLocation,
                               CONCAT(groupes, groupNO, '-' , serialNO) AS groupes,
                               name,
                               CASE gender
                                 WHEN 1 THEN '乾道'
                                 ELSE '坤道'
                               END AS gender,
                               CASE classes
                                 WHEN 1 THEN '新明'
                                 WHEN 2 THEN '至善'
                                 WHEN 3 THEN '培德'
                                 WHEN 4 THEN '行德'
                                 ELSE '崇德'
                               END AS classes,
                               buddhaHall,
                               district
                          FROM classRoom
                          {strClassesWhereKey}
                          {strGenderWhereKey}
                          {strNameWhereKey}
                          ORDER BY sDate DESC, sTime DESC
                          LIMIT {offset}, {rowCount}
            ";

            var result = conn.Query<T>(sqlStr, whereKey).ToList();

            return result;
        }

        /// <summary>
        /// 查詢班務資料總數
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="classes">班別</param>
        /// <param name="gender">性別</param>
        /// <param name="name">姓名</param>
        /// <returns></returns>
        public T GetClassRoomTotal<T>(string classes = "", string gender = "", string name = "")
        {
            string strClassesWhereKey = (classes != "") ? "WHERE classes = @classes " : "WHERE classes IS NOT NULL ";
            string strGenderWhereKey = (gender != "") ? "AND gender = @gender " : "AND gender IS NOT NULL ";
            string strNameWhereKey = (name != "") ? "AND LOCATE(UPPER(@name), name) " : "AND name IS NOT NULL ";
            object whereKey = new
            {
                classes = classes,
                gender = gender,
                name = name
            };

            sqlStr = $@"SELECT count(*) as total FROM classroom 
                                                 {strClassesWhereKey}
                                                 {strGenderWhereKey}
                                                 {strNameWhereKey}
            ";

            var result = conn.Query<T>(sqlStr, whereKey).FirstOrDefault();

            return result;
        }

        /// <summary>
        /// 檢查班務是否組別重複
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="groupes">組別</param>
        /// <param name="groupNo">組號</param>
        /// <param name="serialNo">編號</param>
        /// <returns></returns>
        public T InspactClassRoomClasses<T>(string groupes, int groupNo, int serialNo)
        {
            sqlStr = @"SELECT name,
                              classes,
                              groupes
                         FROM classRoom
                        WHERE     groupes  = @groupes
                              AND groupNo  = @groupNo
                              AND serialNo = @serialNo
            ";

            object whereKey = new {
                groupes = groupes,
                groupNo = groupNo,
                serialNo = serialNo
            };

            var result = conn.Query<T>(sqlStr, whereKey).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 檢查一筆班務資料
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="classRoomId">班務 id</param>
        /// <returns></returns>
        public T InspactOnlyClassRoom<T>(int classRoomId)
        {
            sqlStr = @"SELECT classRoomId,
                              classLocation,
                              groupes,
                              groupNo,
                              serialNo,
                              name,
                              gender,
                              classes,
                              buddhaHall,
                              district,
                              mUser
                         FROM classRoom WHERE classRoomId = @classRoomId
            ";

            object whereKey = new {
                classRoomId = classRoomId
            };

            var result = conn.Query<T>(sqlStr, whereKey).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 新增班務資料
        /// </summary>
        /// <param name="obj">新增班務模型</param>
        /// <returns></returns>
        public int InsertClassRoom(InsertClassRoomForm obj)
        {
            string groupes = obj.groupes;                              //組別
            int groupNo = obj.groupNo;                                 //組號
            int serialNo = obj.serialNo;                               //編號
            string classLocation = obj.classLocation;                  //上課地點
            string name = obj.name;                                    //姓名
            string gender = obj.gender;                                //性別
            string classes = obj.classes;                              //班別
            string buddhaHall = obj.buddhaHall;                        //所屬佛院
            string district = obj.district;                            //所屬區別
            string nowUser = obj.nowUser;                              //建立使用者
            string nowDate = DateTime.Now.ToString("yyyyMMdd");        //建立日期與修改日期
            string nowTime = DateTime.Now.ToString("HHmmss");          //建立時間與修改時間

            sqlStr = @"INSERT INTO classRoom 
                    ( groupes,  groupNo,  serialNo,  classLocation,  name,  gender,  classes,  buddhaHall,  district,  sUser,  sDate,  sTime,  mUser,  mDate,  mTime) VALUES 
                    (@groupes, @groupNO, @serialNo, @classLocation, @name, @gender, @classes, @buddhaHall, @district, @sUser, @sDate, @sTime, @mUser, @mDate, @mTime)
            ";

            object paramKey = new {
                groupes = groupes,
                groupNo = groupNo,
                serialNo = serialNo,
                classLocation = classLocation,
                name = name,
                gender = gender,
                classes = classes,
                buddhaHall = buddhaHall,
                district = district,
                sUser = nowUser,
                sDate = nowDate,
                sTime = nowTime,
                mUser = nowUser,
                mDate = nowDate,
                mTime = nowTime
            };

            var rowCount = conn.Execute(sqlStr, paramKey);

            return rowCount;
        }

        /// <summary>
        /// 更新班務資料
        /// </summary>
        /// <param name="obj">更新班務模型</param>
        /// <returns></returns>
        public int UpdataClassRoom(UpdateClassRoomForm obj)
        {
            int classRoomId = obj.classRoomId;                           //系統公告 id
            int groupNo = obj.groupNo;                                   //組號
            int serialNo = obj.serialNo;                                 //編號
            string groupes = obj.groupes;                                //組別
            string classLocation = obj.classLocation;                    //上課地點
            string name = obj.name;                                      //姓名
            string gender = obj.gender;                                  //性別
            string classes = obj.classes;                                //班別
            string buddhaHall = obj.buddhaHall;                          //所屬佛院
            string district = obj.district;                              //所屬區別
            string mUser = obj.mUser;                                    //修改使用者
            string nowDate = DateTime.Now.ToString("yyyyMMdd");          //修改日期
            string nowTime = DateTime.Now.ToString("HHmmss");            //修改時間

            sqlStr = @"UPDATE classRoom SET classLocation = @classLocation,
                                            groupes       = @groupes,
                                            groupNo       = @groupNo,
                                            serialNo      = @serialNo,
                                            name          = @name,
                                            gender        = @gender,
                                            classes       = @classes,
                                            buddhaHall    = @buddhaHall,
                                            district      = @district,
                                            mUser         = @mUser,
                                            mDate         = @mDate,
                                            mTime         = @mTime
                                        WHERE classRoomId = @classRoomId
            ";

            object paramKey = new {
                classLocation = classLocation,
                groupes = groupes,
                groupNo = groupNo,
                serialNo = serialNo,
                name = name,
                gender = gender,
                classes = classes,
                buddhaHall = buddhaHall,
                district = district,
                mUser = mUser,
                mDate = nowDate,
                mTime = nowTime,
                classRoomId = classRoomId
            };

            var rowCount = conn.Execute(sqlStr, paramKey);

            return rowCount;
        }

        /// <summary>
        /// 刪除班務資料
        /// </summary>
        /// <param name="classRoomId">班務 id</param>
        /// <returns></returns>
        public int DeleteClassRoom(int classRoomId)
        {
            sqlStr = "DELETE FROM classRoom WHERE classRoomId = @classRoomId";

            object paramKey = new {
                classRoomId = classRoomId
            };

            var rowCount = conn.Execute(sqlStr, paramKey);

            return rowCount;
        }

        public IEnumerable<T> ReportClassRoom<T>(string classes, string gender, string name)
        {
            string strClassesWhereKey = (classes != "") ? "WHERE classes = @classes " : "WHERE classes IS NOT NULL ";
            string strGenderWhereKey = (gender != "") ? "AND gender = @gender " : "AND gender IS NOT NULL ";
            string strNameWhereKey = (name != "") ? "AND LOCATE(UPPER(@name), name) " : "AND name IS NOT NULL ";
            object whereKey = new {
                classes = classes,
                gender = gender,
                name = name
            };

            sqlStr = $@"SELECT classRoomId,
                               classLocation,
                               CONCAT(groupes, groupNO, '-' , serialNO) AS groupes,
                               name,
                               CASE gender
                                 WHEN 1 THEN '乾道'
                                 ELSE '坤道'
                               END AS gender,
                               CASE classes
                                 WHEN 1 THEN '新明'
                                 WHEN 2 THEN '至善'
                                 WHEN 3 THEN '培德'
                                 WHEN 4 THEN '行德'
                                 ELSE '崇德'
                               END AS classes,
                               buddhaHall,
                               district
                          FROM classRoom
                          {strClassesWhereKey}
                          {strGenderWhereKey}
                          {strNameWhereKey}
                          ORDER BY sDate DESC, sTime DESC
            ";

            var result = conn.Query<T>(sqlStr, whereKey).ToList();

            return result;
        }

    }

    /*
     ** 接收模型
     */
    
    /// <summary>
    /// 查詢班務資料，基本搜尋資料表單
    /// </summary>
    public class SearchClassRoomBaseForm
    {
        /// <summary>
        /// 班別
        /// </summary>
        [RegularExpression("[1-5]{1}$")]
        public string classes { get; set; }

        /// <summary>
        /// 性別: 1. 乾道，0. 坤道
        /// </summary>
        [RegularExpression("(1|0){1}$")]
        public string gender { get; set; }

        /// <summary>
        /// 頁碼
        /// </summary>
        [Required]
        [RegularExpression("[0-9]*$")]
        public int page { get; set; }

        /// <summary>
        /// 筆數
        /// </summary>
        [Required]
        [RegularExpression("(10|20|30|50|100)$")]
        public int itemBarMenu { get; set; }
    }

    /// <summary>
    /// 查詢班務詳細資料，詳細搜尋資料表單
    /// </summary>
    public class SearchClassRoomDetailForm
    {
        /// <summary>
        /// 班別
        /// </summary>
        [RegularExpression("[1-5]{1}$")]
        public string classes { get; set; }

        /// <summary>
        /// 性別: 1. 乾道，0. 坤道
        /// </summary>
        [RegularExpression("(1|0){1}$")]
        public string gender { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        [RegularExpression("(.*?){3,6}$")]
        public string name { get; set; }

        /// <summary>
        /// 頁碼
        /// </summary>
        [Required]
        [RegularExpression("[0-9]*$")]
        public int page { get; set; }

        /// <summary>
        /// 筆數
        /// </summary>
        [Required]
        [RegularExpression("(10|20|30|50|100)$")]
        public int itemBarMenu { get; set; }
    }

    /// <summary>
    /// 查詢一筆班務資料表單
    /// </summary>
    public class SearchClassRoomIdForm
    {
        /// <summary>
        /// 班務 id
        /// </summary>
        [Required]
        [RegularExpression("[0-9]*$")]
        public int classRoomId { get; set; }
    }

    /// <summary>
    /// 新增班務資料表單
    /// </summary>
    public class InsertClassRoomForm
    {
        /// <summary>
        /// 上課地點
        /// </summary>
        [Required]
        [RegularExpression("(.*?){4}$")]
        public string classLocation { get; set; }
        
        /// <summary>
        /// 組別
        /// </summary>
        [Required]
        [RegularExpression("(仁|義|禮|智|信){1}$")]
        public string groupes { get; set; }

        /// <summary>
        /// 組號
        /// </summary>
        [Required]
        [RegularExpression("[0-9]*$")]
        public int groupNo { get; set; }

        /// <summary>
        /// 編號
        /// </summary>
        [Required]
        [RegularExpression("[0-9]*$")]
        public int serialNo { get; set; }
        
        /// <summary>
        /// 姓名
        /// </summary>
        [Required]
        [RegularExpression("(.*?){3,6}$")]
        public string name { get; set; }
        
        /// <summary>
        /// 性別
        /// </summary>
        [Required]
        [RegularExpression("(1|0)$")]
        public string gender { get; set; }
        
        /// <summary>
        /// 班別
        /// </summary>
        [Required]
        [RegularExpression("[0-9]*$")]
        public string classes { get; set; }
        
        /// <summary>
        /// 所屬佛堂
        /// </summary>
        [Required]
        [RegularExpression("(.*?){4}$")]
        public string buddhaHall { get; set; }
        
        /// <summary>
        /// 所屬區別
        /// </summary>
        [Required]
        [RegularExpression("(.*?){4}$")]
        public string district { get; set; }

        /// <summary>
        /// 建立使用者
        /// </summary>
        [Required]
        [RegularExpression("(.*?){3,6}$")]
        public string nowUser { get; set; }
    }

    /// <summary>
    /// 更新班務資料表單
    /// </summary>
    public class UpdateClassRoomForm
    {
        /// <summary>
        /// 班務 id
        /// </summary>
        [Required]
        [RegularExpression("[0-9]*$")]
        public int classRoomId { get; set; }

        /// <summary>
        /// 上課地點
        /// </summary>
        [Required]
        [RegularExpression("(.*?){4}$")]
        public string classLocation { get; set; }

        /// <summary>
        /// 組別
        /// </summary>
        [Required]
        [RegularExpression("(仁|義|禮|智|信){1}$")]
        public string groupes { get; set; }

        /// <summary>
        /// 組號
        /// </summary>
        [Required]
        [RegularExpression("[0-9]*$")]
        public int groupNo { get; set; }

        /// <summary>
        /// 編號
        /// </summary>
        [Required]
        [RegularExpression("[0-9]*$")]
        public int serialNo { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        [Required]
        [RegularExpression("(.*?){3,6}$")]
        public string name { get; set; }

        /// <summary>
        /// 性別: 1. 乾道，0.坤道
        /// </summary>
        [Required]
        [RegularExpression("(1|0)$")]
        public string gender { get; set; }

        /// <summary>
        /// 班別
        /// </summary>
        [Required]
        [RegularExpression("[0-9]*$")]
        public string classes { get; set; }

        /// <summary>
        /// 所屬佛院
        /// </summary>
        [Required]
        [RegularExpression("(.*?){4}$")]
        public string buddhaHall { get; set; }

        /// <summary>
        /// 所屬區別
        /// </summary>
        [Required]
        [RegularExpression("(.*?){4}$")]
        public string district { get; set; }

        /// <summary>
        /// 更新使用者
        /// </summary>
        [Required]
        [RegularExpression("(.*?){3,6}$")]
        public string mUser { get; set; }
    }

    /// <summary>
    /// 班務報表輸出
    /// </summary>
    public class ReportClassRoomForm
    {
        /// <summary>
        /// 班別
        /// </summary>
        [RegularExpression("[1-5]{1}$")]
        public string classes { get; set; }

        /// <summary>
        /// 性別: 1. 乾道，0. 坤道
        /// </summary>
        [RegularExpression("(1|0){1}$")]
        public string gender { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        [RegularExpression("(.*?){3,6}$")]
        public string name { get; set; }
    }
}