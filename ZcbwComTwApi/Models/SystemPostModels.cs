﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;

using MySql.Data.MySqlClient;
using Dapper;

namespace ZcbwComTwApi.Models
{
    public class SystemPostModels
    {
        private MySqlConnection conn;
        private string sqlStr = string.Empty;

        public SystemPostModels(MySqlConnection _conn)
        {
            conn = _conn;
        }

        /// <summary>
        /// 查詢系統公告，基本搜尋資料
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="publish">發佈狀態</param>
        /// <param name="offset">頁碼</param>
        /// <param name="rowCount">筆數</param>
        /// <returns></returns>
        public IEnumerable<T> GetSystemPostBase<T>(string publish, int offset, int rowCount)
        {
            string strPublishWhereKey = (publish == "") ? "WHERE publish IS NOT NULL " : "WHERE publish = @publish ";
            object whereKey = new { publish = publish };

            sqlStr = $@"SELECT systemPostId,
                               postTitle,
                               postMessage,
                               publish,
                               sDate,
                               sTime
                          FROM systempost
                            {strPublishWhereKey}
                            ORDER BY sDate DESC, sTime DESC
                            LIMIT {offset}, {rowCount}
            ";
            
            var result = conn.Query<T>(sqlStr, whereKey).ToList();

            return result;
        }

        /// <summary>
        /// 取得系統公告，詳細搜尋資料
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="publish">發佈狀態</param>
        /// <param name="postTitle">系統標題</param>
        /// <param name="offset">頁碼</param>
        /// <param name="rowCount">筆數</param>
        /// <returns></returns>
        public IEnumerable<T> GetSystemPostDetail<T>(string publish, string postTitle, int offset, int rowCount)
        {
            string strPublishWhereKey = (publish == "") ? "WHERE publish IS NOT NULL " : "WHERE publish = @publish ";
            string strTitleWhereKey = (postTitle == "") ? "AND postTitle IS NOT NULL " : "AND LOCATE(UPPER(@postTitle), postTitle) ";
            object whereKey = new { publish = publish, postTitle = postTitle };

            sqlStr = $@"SELECT systemPostId,
                               postTitle,
                               postMessage,
                               publish,
                               sDate,
                               sTime
                          FROM systempost
                            {strPublishWhereKey}
                            {strTitleWhereKey}
                            ORDER BY sDate DESC, sTime DESC
                            LIMIT {offset}, {rowCount}
            ";

            var result = conn.Query<T>(sqlStr, whereKey).ToList();

            return result;
        }


        /// <summary>
        /// 取得系統公告總數
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="publish">發佈狀態</param>
        /// <param name="postTitle">系統標題</param>
        /// <returns></returns>
        public T GetSystemPostTotal<T>(string publish = "", string postTitle = "")
        {
            string strPublishWhereKey = (publish == "") ? "WHERE publish IS NOT NULL " : "WHERE publish = @publish ";
            string strPostTitleWhereKey = (postTitle == "") ? "AND postTitle IS NOT NULL " : "AND LOCATE(UPPER(@postTitle), postTitle) ";
            object whereKey = new { publish = publish, postTitle = postTitle };

            sqlStr = $@"SELECT COUNT(*) as total FROM systemPost
                                                 {strPublishWhereKey}
                                                 {strPostTitleWhereKey}
            ";

            var result = conn.Query<T>(sqlStr, whereKey).FirstOrDefault();

            return result;
        }

        /// <summary>
        /// 取得一筆系統公告
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="systemPostId">系統公告 id</param>
        /// <returns></returns>
        public T InspactSystemPost<T>(int systemPostId)
        {
            sqlStr = @"SELECT systemPostId, postTitle, postMessage, publish, mUser FROM systemPost WHERE systemPostId = @systemPostId";

            object whereKey = new
            {
                systemPostId = systemPostId
            };

            var result = conn.Query<T>(sqlStr, whereKey).SingleOrDefault();

            return result;
        }

        /// <summary>
        /// 新增系統公告
        /// </summary>
        /// <param name="obj">新增系統公告模型</param>
        /// <returns></returns>
        public int InsertSystemPost(InsertSystemPostForm obj)
        {
            string postTitle = obj.postTitle;                        //公告標題
            string postMessage = obj.postMessage;                    //公告訊息
            string publish = obj.publish;                            //發佈狀態
            string nowUser = obj.nowUser;                            //建立使用者與修改使用者
            string nowDate = DateTime.Now.ToString("yyyyMMdd");      //建立日期與修改日期
            string nowTime = DateTime.Now.ToString("HHmmss");        //建立時間與修改時間
            sqlStr = @"INSERT INTO systemPost ( postTitle,  postMessage,  publish,  sUser,  sDate,  sTime,  mUser,  mDate,  mTime) VALUES 
                                              (@postTitle, @postMessage, @publish, @sUser, @sDate, @sTime, @mUser, @mDate, @mTime)
            ";

            object param = new
            {
                postTitle = postTitle,
                postMessage = postMessage,
                publish = publish,
                sUser = nowUser,
                sDate = nowDate,
                sTime = nowTime,
                mUser = nowUser,
                mDate = nowDate,
                mTime = nowTime
            };

            var rowCount = conn.Execute(sqlStr, param);

            return rowCount;
        }


        /// <summary>
        /// 更新系統公告
        /// </summary>
        /// <param name="obj">更新系統公告模型</param>
        /// <returns></returns>
        public int UpdateSystemPost(UpdateSystemPostForm obj)
        {
            sqlStr = @"UPDATE systemPost SET postTitle   = @postTitle,
                                             postMessage = @postMessage,
                                             publish     = @publish,
                                             mUser       = @mUser        WHERE systemPostId = @systemPostId
            ";

            var whereKey = new
            {
                postTitle = obj.postTitle,
                postMessage = obj.postMessage,
                publish = obj.publish,
                mUser = obj.mUser,
                systemPostId = obj.systemPostId
            };

            var rowCount = conn.Execute(sqlStr, whereKey);

            return rowCount;
        }

        /// <summary>
        /// 更新系統公告發佈狀態
        /// </summary>
        /// <param name="obj">更新系統公告狀態模型</param>
        /// <returns></returns>
        public int UpdateSystemPostPublishStatus(UpdateSystemPostPublishStatusForm obj)
        {
            int systemPostId = obj.systemPostId;         //系統公告 id
            int publish = (obj.publish == 1) ? 0 : 1;    //發佈狀態
            string mUser = obj.mUser;                    //修改使用者

            sqlStr = @"UPDATE systemPost SET publish = @publish,
                                             mUser   = @mUser      WHERE systemPostId = @systemPostId";

            object whereKey = new {
                publish = publish,
                mUser = mUser,
                systemPostId = systemPostId
            };

            var rowCount = conn.Execute(sqlStr, whereKey);

            return rowCount;
        }


        /// <summary>
        /// 刪除系統公告
        /// </summary>
        /// <param name="systemPostId">系統公告 id</param>
        /// <returns></returns>
        public int DeleteSystemPost(int systemPostId)
        {
            sqlStr = @"DELETE FROM systemPost WHERE systemPostId = @systemPostId";
            object whereKey = new { systemPostId = systemPostId };

            var rowCount = conn.Execute(sqlStr, whereKey);

            return rowCount;
        }
    }

    /**
     * 接收模型
     */

    /// <summary>
    /// 查詢系統公告，基本搜尋表單
    /// </summary>
    public class SearchSystemPostBaseForm
    {
        /// <summary>
        /// 發佈狀態
        /// </summary>
        [RegularExpression("(''|1|0)$")]
        public string publish { get; set; }

        /// <summary>
        /// 頁碼
        /// </summary>
        [Required]
        [RegularExpression("[0-9]*$")]
        public int page { get; set; }

        /// <summary>
        /// 筆數
        /// </summary>
        [Required]
        [RegularExpression("(10|20|30|50|100)$")]
        public int itemBarMenu { get; set; }
    }

    /// <summary>
    /// 查詢系統公告，詳細搜尋表單
    /// </summary>
    public class SearchSystemPostDetailForm
    {
        /// <summary>
        /// 發佈狀態
        /// </summary>
        [RegularExpression("(''|1|0)$")]
        public string publish { get; set; }

        /// <summary>
        /// 公告標題
        /// </summary>
        public string postTitle { get; set; }

        /// <summary>
        /// 頁碼
        /// </summary>
        [Required]
        [RegularExpression("[0-9]$")]
        public int page { get; set; }

        /// <summary>
        /// 筆數
        /// </summary>
        [Required]
        [RegularExpression("(10|20|30|50|100)$")]
        public int itemBarMenu { get; set; }
    }

    /// <summary>
    /// 查詢一筆系統公告表單
    /// </summary>
    public class SearchOnlySystemPostForm
    {
        /// <summary>
        /// 系統公告 id
        /// </summary>
        [Required]
        [RegularExpression("[0-9]*$")]
        public int systemPostId { get; set; }
    }

    /// <summary>
    /// 新增系統公告表單
    /// </summary>
    public class InsertSystemPostForm
    {
        /// <summary>
        /// 公告標題
        /// </summary>
        [Required]
        [RegularExpression("(.*?){1,50}$")]
        public string postTitle { get; set; }

        /// <summary>
        /// 公告訊息
        /// </summary>
        [RegularExpression("(.*?){1,200}$")]
        public string postMessage { get; set; }

        /// <summary>
        /// 發佈狀態
        /// </summary>
        [Required]
        [RegularExpression("(1|0){1}$")]
        public string publish { get; set; }

        /// <summary>
        /// 建立使用者
        /// </summary>
        [Required]
        [RegularExpression("(.*?){3,6}$")]
        public string nowUser { get; set; }
    }

    /// <summary>
    /// 更新系統公告表單
    /// </summary>
    public class UpdateSystemPostForm
    {
        /// <summary>
        /// 系統公告 id
        /// </summary>
        [Required]
        [RegularExpression("[0-9]*$")]
        public int systemPostId { get; set; }

        /// <summary>
        /// 公告標題
        /// </summary>
        [Required]
        [RegularExpression("(.*?){1,50}$")]
        public string postTitle { get; set; }

        /// <summary>
        /// 公告訊息
        /// </summary>
        [RegularExpression("(.*?){1,200}$")]
        public string postMessage { get; set; }

        /// <summary>
        /// 發佈狀態
        /// </summary>
        [Required]
        [RegularExpression("(1|0){1}$")]
        public string publish { get; set; }

        /// <summary>
        /// 更新使用者
        /// </summary>
        [Required]
        [RegularExpression("(.*?){3,6}$")]
        public string mUser { get; set; }
    }

    /// <summary>
    /// 更新系統公告發佈狀態表單
    /// </summary>
    public class UpdateSystemPostPublishStatusForm
    {
        /// <summary>
        /// 系統公告 id
        /// </summary>
        [Required]
        [RegularExpression("[0-9]*$")]
        public int systemPostId { get; set; }

        /// <summary>
        /// 發佈狀態
        /// </summary>
        [Required]
        [RegularExpression("(1|0){1}$")]
        public int publish { get; set; }

        /// <summary>
        /// 更新使用者
        /// </summary>
        [Required]
        [RegularExpression("(.*?){3,6}$")]
        public string mUser { get; set; }
    }

    /// <summary>
    /// 刪除系統公告
    /// </summary>
    public class DeleteSystemPostForm
    {
        /// <summary>
        /// 系統公告 id
        /// </summary>
        [Required]
        [RegularExpression("[0-9]*$")]
        public int systemPostId { get; set; }
    }
}