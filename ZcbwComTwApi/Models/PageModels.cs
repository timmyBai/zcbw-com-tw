﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Dapper;
using MySql.Data.MySqlClient;

namespace ZcbwComTwApi.Models
{
    public class PageModels
    {
        private MySqlConnection conn;
        private string sqlStr = string.Empty;

        public PageModels(MySqlConnection _conn)
        {
            conn = _conn;
        }

        public IEnumerable<T> GetEntirePageList<T>()
        {
            sqlStr = "SELECT * FROM webPage";

            var result = conn.Query<T>(sqlStr);

            return result;
        }
    }
}