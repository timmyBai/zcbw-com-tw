import router from './router';
import store from './store';
import getPageTitle from '@/utils/getTitlePage.js';
import { getToken } from '@/utils/auth.js';

const whiteList = ['/login', '/404'];

router.beforeEach(async(to, from, next) => {
    document.title = getPageTitle(to.meta.title);

    const hasToken = getToken();

    if (hasToken)
    {
        if (to.path === '/login')
        {
            next({ path: '/' });
        }
        else
        {
            //確認有無需要加入新的 router 
            const hasRoles = store.getters.roles && store.getters.roles.length > 0;

            if (hasRoles)
            {
                next();
            }
            else
            {
                try
                {
                    const { roles } = await store.dispatch('user/getInfo');
                    
                    const asccessRoute = await store.dispatch('permission/generateRoutes', roles);
                    
                    router.addRoutes(asccessRoute);

                    next({ ...to, replace: true });
                }
                catch(error)
                {
                    await store.dispatch('user/resetToken');

                    next({path: `/login?redirect=${to.path}`});
                }
            }
        }
    }
    else
    {
        if (whiteList.indexOf(to.path) !== -1)
        {
            next();
        }
        else
        {
            next(`/login?redirect=${to.path}`);
        }
    }
})

router.afterEach(() => {
});
