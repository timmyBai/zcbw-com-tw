import { setToken, getToken, removeToken } from '@/utils/auth.js';
import { login } from '@/api/login.js';
import { Introduction } from '@/api/introduction.js';
import { resetRouter } from '@/router';

const state = {
    token: getToken(),
    userName: '',
    roles: []
}

const mutations = {
    SET_TOKEN(state, token) {
        state.token = token;
    },
    SET_USER(state, user) {
        state.userName = user;
    },
    SET_ROLES(state, roles) {
        state.roles = roles;
    }
}

const actions = {
    login({commit}, userInfo) {
        const { account, password } = userInfo;

        return new Promise((resolve, reject) => {
            login(account, password).then((res) => {
                const { data } = res;
                
                if (res.status === 200)
                {
                    if (res.data.isSuccess === true)
                    {
                        setToken(data.data.token);

                        commit('SET_TOKEN', data.data.token);
                        resolve();
                    }
                }
            }).catch((error) => {
                reject(error);
            });
        });
    },
    getInfo({commit, state}) {
        return new Promise((resolve, reject) => {
            Introduction(state.token).then((res) => {
                const { data } = res;
                
                if (res.status === 200)
                {
                    if (data.isSuccess === true)
                    {
                        const { roles, userName } = data.data;

                        commit('SET_USER', userName);
                        commit('SET_ROLES', roles);

                        resolve(data.data);
                    }
                }
            }).catch((error) => {
                reject(error);
            });
        });
    },
    logout({commit}) {
        return new Promise((resolve) => {
            commit('SET_TOKEN', '');
            commit('SET_USER', '');
            commit('SET_ROLES', []);

            removeToken();

            resetRouter();

            resolve();
        });
    },
    resetToken({commit}) {
        return new Promise(resolve => {
            commit('SET_TOKEN', '');
            commit('SET_ROLES', []);
            commit('SET_USER', '');

            removeToken();
            resolve();
        })
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}