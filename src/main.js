import '@babel/polyfill'

import Vue from 'vue';
import App from './App.vue';
import store from './store';
import router from './router';

//js
import './permission.js';
import 'bootstrap';
import 'jquery';
import 'popper.js';
import SimpleVueValidation from 'simple-vue-validator';



import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
library.add(fas, fab);

//css
import 'normalize.css';
import 'bootstrap/scss/bootstrap.scss';
import '@/styles/reset.sass';

//元件註冊
import Loading from './components/Loading/index.vue';

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('Loading', Loading);

//套件使用
Vue.use(SimpleVueValidation);


/** elemenu ui */
import Element from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'element-ui/lib/index.js';

Vue.use(Element);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
