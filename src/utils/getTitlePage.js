import defaultSettings from '@/settings';

const title = defaultSettings.title || '正承佛院資訊系統'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`;
  }
  
  return `${title}`;
}