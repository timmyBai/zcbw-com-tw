import Layout from '@/layout';

const classRoomRouter = {
    path: '/classRoomMangement',            //班務管理系統
    redirect: '/classroomMangement/index',
    component: Layout,
    children: [
      {
        path: 'index',
        component: () => import('@/views/classroomMangement/index.vue'),
        name: 'classroom',
        meta: {
          title: '班務管理系統', roles: ['supper']
        }
      },
      {
        path: 'new',
        component: () => import('@/views/classroomMangement/newClassRoomMangement.vue'),
        name: 'newClassRoom',
        meta: {
          title: '新增班務資料', roles: ['supper']
        },
        hidden: true
      },
      {
        path: 'md/:classRoomId',
        component: () => import('@/views/classroomMangement/mdClassRoomMangement.vue'),
        meta: {
          title: '修改班務資料', roles: ['supper']
        },
        hidden: true
      }
    ]
};

export default classRoomRouter;