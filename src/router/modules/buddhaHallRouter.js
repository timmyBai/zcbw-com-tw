import Layout from '@/layout';

const buddhaHallRouter = {
    path: '/buddhaHall',
    redirect: '/buddhaHall/index',
    component: Layout,
    children: [
        {
            path: 'index',
            component: () => import('@/views/buddhaHall/index.vue'),
            name: 'buddhaHall',
            meta: {
                title: '佛院管理系統', roles: ['supper']
            }
        }
    ]
}

export default buddhaHallRouter;