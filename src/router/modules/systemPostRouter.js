import Layout from '@/layout';

const systemPostRouter = {
  path: '/systemPost',
  redirect: '/systemPost/index',
  component: Layout,
  children: [
    {
      path: 'index',
      component: () => import('@/views/systemPost/index.vue'),
      name: 'systemPost',
      meta: {
        title: '系統公告', roles: ['supper']
      }
    },
    {
      path: 'new',
      component: () => import('@/views/systemPost/newSystemPost.vue'),
      name: 'systemPostNew',
      meta: {
        title: '新增公告', roles: ['supper']
      },
      hidden: true
    },
    {
      path: 'md/:systemPostId',
      component: () => import('@/views/systemPost/mdSystemPost.vue'),
      name: 'systemPostMod',
      meta: {
        title: '系統公告修改', roles: ['supper']
      },
      hidden: true
    }
  ]
};

export default systemPostRouter;