import request from '@/utils/request.js';

export function Introduction(token)
{
    return request({
        url: 'api/UserIntroduction',
        method: 'post',
        data: {
            token
        }
    })
}