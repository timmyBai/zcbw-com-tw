import request from '@/utils/request';

export function login(account, password) {
  return request({
    url: 'api/LoginUser',
    method: 'post',
    data: {
      account,
      password
    }
  });
}