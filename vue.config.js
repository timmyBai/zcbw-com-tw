'use strict'
const path = require('path');
const defaultSettings = require('./src/settings.js')

function resolve(dir) {
  return path.join(__dirname, dir)
}

const name = defaultSettings.title || '正承佛院資訊系統' // page title
const port = Math.floor(Math.random(1000) * 9999);

module.exports = {
  publicPath: '/zcbw/',
  outputDir: 'dist',
  assetsDir: 'static',
  productionSourceMap: true,
  devServer: {
    open: true,
    port: port
  },
  configureWebpack: {
    name: name,
    resolve: {
      alias: {
        '@': resolve('src')
      }
    }
  },
  transpileDependencies: ["@vue", "vue", "vue-router", "vuex"]
}